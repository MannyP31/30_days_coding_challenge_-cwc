def is_palindrome(n):
   
    to_str = str(n)
    if to_str == to_str[::-1]:
        return True
    return False


def find_palindromes(n):
  
    decimal_binary = {decimal: bin(decimal)[2:] for decimal in range(1, n) if is_palindrome(decimal)}
    for decimal, binary in decimal_binary.items():
        if is_palindrome(binary) and not binary[0] == 0:
            yield decimal


if __name__ == '__main__':
    print(sum(list(find_palindromes(1000000))))