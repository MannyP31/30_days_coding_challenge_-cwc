#include <bits/stdc++.h>
using namespace std;

float harmonic_mean(float arr[], int size){
   float sum = 0;
   for (int i = 0; i < size; i++)
      sum = sum + (float)1 / arr[i];
   return (float)size/sum;
}
int main(){
   float arr[] = {4.2, 5.4, 2.3, 3.1};
   int size = sizeof(arr) / sizeof(arr[0]);
   cout<<"Harmonic mean is : "<<harmonic_mean(arr, size);
   return 0;
}