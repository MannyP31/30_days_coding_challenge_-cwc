def set_bit_count(num) :
   cnt = 0
   while num:
      cnt += num & 1
      num >>= 1
   return cnt
def solve(m, n) :
   if set_bit_count(m) == set_bit_count(n):
      return True
   return False
m = 8
n = 4
print(solve(m, n))